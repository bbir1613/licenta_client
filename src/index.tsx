import * as React from "react";
import * as ReactDOM from "react-dom";
import {Router, Route} from "react-router"
import {Provider} from "react-redux"
import {createHashHistory} from "history"

// import Initial from "./components/Initial";
import Overlay from "./overlay/Overlay";

import store from "./store"

ReactDOM.render((
        <Provider store={store}>
            <Router history={createHashHistory()}>
                <Route path="/" component={Overlay}/>
            </Router>
        </Provider>
    ),
    document.getElementById("example")
);