export class ReducerActionTypes {
    static IMAGE_UPDATED: string = '@ReducerActionTypes/IMAGE_UPDATED';
    static IMAGE_LOADED: string = '@ReducerActionTypes/IMAGE_LOADED';
}