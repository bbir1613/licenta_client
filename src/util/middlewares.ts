import {MyAction} from "./utils";

const middleWare: any = (store: any) => (next: any) => (action: MyAction) => {
    // console.log(" middleWare " + JSON.stringify(action));
    action instanceof MyAction ? next(action.toJson()) : next(action)
};

export {middleWare}
