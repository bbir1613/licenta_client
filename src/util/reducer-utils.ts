import {Action} from "redux";
import {MyAction} from "./utils";

export interface IReducer {
    reducer: (state: any, action: Action) => any;
}

export abstract class AbstractReducer {
    handleActions: any = {};

    constructor(ActionsType: any) {
        // console.log("AbstractReducer constructor");
        Object.getOwnPropertyNames(ActionsType).map(propertyName => {
            if (ActionsType.propertyIsEnumerable(propertyName)) {
                // console.log(ActionsType[propertyName]);
                this.handleActions[ActionsType[propertyName]] = this.actionHandler;
            }
        });
        // console.log("AbstractReducer constructor", this.handleActions);
    }

    private actionHandler(state: any, action: MyAction): any {
        return action.payload ? {...state, ...action.payload} : {...state};
    }
}