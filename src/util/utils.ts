import {Action} from "redux";
import {IReducer} from "./reducer-utils";

export class MyAction implements Action {
    type: string;
    payload: any;

    constructor(type: string = '', payload: {} = undefined) {
        this.type = type;
        this.payload = payload;
    }

    toJson() {
        return {type: this.type, payload: this.payload};
    }
}

export function handleReducer(r: IReducer) {
    return (state: any, action: Action) => r.reducer(state, action);
}
