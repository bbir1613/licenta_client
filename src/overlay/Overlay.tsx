import * as React from "react";
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {MuiThemeProvider, lightBaseTheme} from "material-ui/styles";
import AppBar from "material-ui/AppBar"
import Initial from "../components/Initial"

const lightMuiTheme = getMuiTheme(lightBaseTheme);

export default class Overlay extends React.Component<any, any> {

    render() {
        return (<MuiThemeProvider muiTheme={lightMuiTheme}>
            <div>
                <AppBar
                    title="Upload image"
                    iconElementLeft={undefined}
                />
                <Initial/>
            </div>
        </MuiThemeProvider>)
    }
}
