import * as React from "react";
import {reducerStateInterface} from "../reducers/reducer";

export interface HelloProps extends reducerStateInterface {
}

export class Hello extends React.Component<HelloProps, undefined> {

    render() {
        if (this.props.human_string && this.props.score) {
            return <h1>Human string : {this.props.human_string} and score : {this.props.score}!</h1>;
        } else {
            return <div/>
        }
    }
}