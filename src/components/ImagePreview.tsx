import * as React from "react";
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';

export interface ImagePreviewProps {
    imagePreviewUrl: any
}

export class ImagePreview extends React.Component<ImagePreviewProps, undefined> {

    constructor(props: any) {
        super(props);
    }

    render() {
        return (
            <Card>
                <CardHeader
                    title="Image"
                />
                {this.props.imagePreviewUrl ? (
                    <CardMedia>
                        <img src={this.props.imagePreviewUrl}/>
                    </CardMedia>
                ) : (
                    <CardText>
                        Upload image
                    </CardText>)}
            </Card>)
    }
}