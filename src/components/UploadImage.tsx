import * as React from "react";

export interface UploadImageState {
    image: any;
}

export interface UploadImageProps {
    uploadImage: any
    imageLoaded: any
}

export class UploadImage extends React.Component<UploadImageProps, UploadImageState> {

    constructor(props: any) {
        super(props);
        this.state = {
            image: undefined,
        };
    }

    loadImage(event: any) {
        event.preventDefault();
        let reader = new FileReader();
        let image = event.target.files[0];
        reader.onloadend = () => {
            this.setState({
                image: image,
            }, () => {
                this.props.imageLoaded(reader.result)
            })
        };
        reader.readAsDataURL(image)
    }

    render() {
        return (
            <div>
                <div>
                    <h1>Upload the image</h1>
                    <input
                        type="file"
                        name="file"
                        onChange={(e) => this.loadImage(e)}
                    />
                    <button onClick={() => this.props.uploadImage(this.state.image)}>Recognize image!</button>
                </div>
            </div>
        )
    }
}