import * as React from "react";
import {connect} from "react-redux"
import {Hello} from "./Hello"
import {reducerActions, reducerStateInterface, Reducer} from "../reducers/reducer"
import {UploadImage} from "./UploadImage";
import {ImagePreview} from "./ImagePreview";

interface InitialProps extends reducerStateInterface, reducerActions {
}

class Initial extends React.Component<InitialProps, any> {
    constructor(props: any) {
        super(props);
    }

    componentWillMount() {

    }

    render() {
        return <div>
            <Hello human_string={this.props.human_string} score={this.props.score}/>
            <UploadImage uploadImage={this.props.uploadImage} imageLoaded={this.props.imageLoaded}/>
            <ImagePreview imagePreviewUrl={this.props.imagePreviewUrl}/>
        </div>
    }

    componentDidMount() {
    }

    componentWillUnmount() {

    }
}

export default connect<any, any, any>(
    state => {
        return {
            human_string: state.reducer.human_string,
            score: state.reducer.score,
            imagePreviewUrl: state.reducer.imagePreviewUrl
        };
    }
    , dispatch => ({
        uploadImage: (image: any) => Reducer.uploadImage(dispatch, image),
        imageLoaded: (imagePreviewUrl: any) => dispatch(Reducer.imageLoaded(imagePreviewUrl))
    }))(Initial);