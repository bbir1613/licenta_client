import {combineReducers} from "redux"
import {Reducer} from "./reducer"
import {handleReducer} from "../util/utils";

export default combineReducers({reducer: handleReducer(new Reducer())});