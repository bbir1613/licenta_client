import {Action} from "redux"
import {ReducerActionTypes} from "../util/constants"
import {MyAction} from "../util/utils";
import {AbstractReducer, IReducer} from "../util/reducer-utils";
import {Config} from "../util/config";

export interface reducerStateInterface {
    human_string?: string;
    score?: number;
    imagePreviewUrl?: string;
}

export interface reducerActions {
    uploadImage: (dispatch: any, image: any) => Action
    imageLoaded: (imagePreviewUrl: any) => Action
}

export class Reducer extends AbstractReducer implements IReducer {
    initialState: reducerStateInterface;

    constructor() {
        super(ReducerActionTypes);
        // console.log("Reducer constructor");
        this.initialState = {
            human_string: undefined,
            score: undefined,
            imagePreviewUrl: undefined
        };
        console.log("Reducer constructor", this.handleActions);
    }

    reducer(state = this.initialState, action: Action) {
        return this.handleActions[action.type] ? this.handleActions[action.type](state, action) : state;
    }

    private static imageUploaded(response: JSON) {
        return new MyAction(ReducerActionTypes.IMAGE_UPDATED, response)
    }

    static imageLoaded(imagePreviewUrl: string) {
        return new MyAction(ReducerActionTypes.IMAGE_LOADED, {imagePreviewUrl: imagePreviewUrl})
    }

    static uploadImage(dispatch: any, image: any): Action {
        const formData = new FormData();
        formData.append('file', image);

        fetch(Config.SERVER_IP, {
            method: 'POST',
            body: formData
        })
            .then((response: any) => response.json())
            .then((response: any) => {
                //TODO(BOGDAN) : proper handle error + set on server proper response which will set headers
                console.log("response ok is : ", response.ok, " response.status is :  ", response.status);
                if (!response.ok) {
                    console.error(" error ", response)
                } else {
                    console.log("upload succeeded ", response);
                    dispatch(this.imageUploaded(response))
                }
            })
            .catch((error: any) => console.error(error));
        return new MyAction();
    }
}