import {applyMiddleware, createStore} from "redux"
import {createLogger} from "redux-logger"
import reducer from "./reducers/index"
import {middleWare} from "./util/middlewares";

export default createStore(reducer, applyMiddleware(middleWare, createLogger()));
